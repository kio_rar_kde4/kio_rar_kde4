/***************************************************************************
 *   Copyright (C) 2004-2006 by Raul Fernandes                             *
 *   rgfbr@yahoo.com.br                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qstringlist.h>
#include <qbitarray.h>
#include <qfile.h>

#include <kdebug.h>
#include <kcomponentdata.h>
#include <klocale.h>
#include <kurl.h>
#include <kprocess.h>
#include <kglobal.h>
#include <kstandarddirs.h>

#include <sys/stat.h>
#include <stdlib.h>
#include "kio_rar.h"
#include "kio_rar.moc"
#include "krar.h"

extern "C" { int KDE_EXPORT kdemain(int argc, char **argv); }

int kdemain(int argc, char **argv)
{
	KComponentData instance( "kio_rar" );

	kDebug(7101) << "*** Starting kio_rar " << endl;

	if (argc != 4) {
		kDebug(7101) << "Usage: kio_rar  protocol domain-socket1 domain-socket2" << endl;
		exit(-1);
	}

	kio_rarProtocol slave(argv[2], argv[3]);
	slave.dispatchLoop();

	kDebug(7101) << "*** kio_rar Done" << endl;
	return 0;
}

kio_rarProtocol::kio_rarProtocol(const QByteArray &pool_socket, const QByteArray &app_socket)
	: SlaveBase("kio_rar", pool_socket, app_socket)
{
	kDebug(7101) << "kio_rarProtocol::kio_rarProtocol()" << endl;
	rarProgram = KGlobal::dirs()->findExe( "rar" );
	if( rarProgram.isNull() )// Check if rar binary is available
	{
		rarProgram = KGlobal::dirs()->findExe( "unrar" );
		if( rarProgram.isNull() )
		{
			error( KIO::ERR_SLAVE_DEFINED, i18n( "Neither rar nor unrar was not found in PATH. You should install one of them to work with this kioslave" ) );
		}
	}
	m_archiveTime = 0L;
	m_archiveFile = 0L;
}


kio_rarProtocol::~kio_rarProtocol()
{
	kDebug(7101) << "kio_rarProtocol::~kio_rarProtocol()" << endl;
}


void kio_rarProtocol::get(const KUrl& url )
{
	kDebug(7101) << "kio_rar::get(const KUrl& url)" << endl ;

	KUrl fileUrl;
	int errorNum;

	errorNum = checkFile( url, fileUrl );
	if( errorNum == 1 )
	{
		// cannot open archive
		return;
	}

	if( errorNum == 2 )
	{
		// it is not a RAR archive
		// send the path to konqueror to redirect to file: kioslave
		redirection( url.path() );
		finished();
		return;
	}

	if( !m_archiveFile->directory()->entry( fileUrl.path() ) )
	{
		error( KIO::ERR_DOES_NOT_EXIST, fileUrl.path() );
		return;
	}

	procShell = new KProcess();
	processed = 0;

	QStringList args;
	args << "p" << "-inul" <<  "-c-" << "-y" << m_archiveFile->fileName() << fileUrl.path().remove( 0, 1 );
	//-ierr -idp

	infoMessage( i18n( "Unpacking file..." ) );

	connect(procShell, SIGNAL(readyRead()), this, SLOT(receivedData()));
	procShell->setProgram(rarProgram, args);
	procShell->setOutputChannelMode(KProcess::OnlyStdoutChannel);
	procShell->setNextOpenMode( QIODevice::ReadOnly | QIODevice::Unbuffered );
	procShell->start();

	if (!procShell->waitForFinished())
	{
		error( KIO::ERR_CANNOT_LAUNCH_PROCESS, url.path() );
	}

	if( procShell->exitStatus() == QProcess::NormalExit )
	{
		//WARNING Non fatal error(s) occurred
		if( procShell->exitCode() == 1 ) warning( i18n( "Non fatal error(s) occurred" ) );
		//FATAL ERROR A fatal error occurred
		if( procShell->exitCode() == 2 ) error( KIO::ERR_SLAVE_DIED, url.url() );
		//CRC ERROR A CRC error occurred when unpacking
		if( procShell->exitCode() == 3 ) error( KIO::ERR_SLAVE_DEFINED, i18n( "CRC failed for file %1" ).arg( url.url() ) );
		//WRITE ERROR Write to disk error
		//for this error we should use ERR_COULD_NOT_WRITE or ERR_DISK_FULL ?
		if( procShell->exitCode() == 5 ) error( KIO::ERR_DISK_FULL, url.url() );
		//OPEN ERROR Open file error
		if( procShell->exitCode() == 6 ) error( KIO::ERR_CANNOT_OPEN_FOR_READING, url.url() );
		//MEMORY ERROR Not enough memory for operation
		if( procShell->exitCode() == 8 ) error( KIO::ERR_OUT_OF_MEMORY, url.url() );
	}

	data( QByteArray() );
	finished();

	delete procShell;
	procShell = 0;
}


/*!
  \fn kio_rarProtocol::listDir( const KUrl & url )
  */
void kio_rarProtocol::listDir( const KUrl & url )
{
	kDebug(7101) << "kio_rar::listDir(const KUrl& url)" << endl ;
	KUrl fileUrl;
	int errorNum;

	errorNum = checkFile( url, fileUrl );
	if( errorNum == 1 )
	{
		// cannot open archive
		return;
	}

	if( errorNum == 2 )
	{
		// it is not a RAR archive
		// send the path to konqueror to redirect to file: kioslave
		redirection( url.path() );
		finished();
		return;
	}

	infoMessage( i18n( "Listing directory..." ) );

	const KArchiveDirectory *directory;
	if( fileUrl.path().isEmpty() )
		directory = m_archiveFile->directory();
	else
		directory = ((const KArchiveDirectory*)(m_archiveFile->directory()->entry( fileUrl.path() )));
	QStringList list = directory->entries();

	KIO::UDSEntry entry;
	const KArchiveEntry *archiveEntry;

	if( list.count() !=  0 )
		for(QStringList::Iterator it = list.begin(); it != list.end(); ++it)
		{
			archiveEntry = directory->entry(*it );
			entry.clear();

			entry.insert(KIO::UDSEntry::UDS_NAME, archiveEntry->name());

			entry.insert(KIO::UDSEntry::UDS_FILE_TYPE, archiveEntry->isDirectory() ? S_IFDIR : S_IFREG);

			entry.insert( KIO::UDSEntry::UDS_ACCESS, archiveEntry->permissions() );

			entry.insert( KIO::UDSEntry::UDS_SIZE, archiveEntry->isDirectory() ? 0L : ((KArchiveFile *)archiveEntry)->size() );

			entry.insert(KIO::UDSEntry::UDS_MODIFICATION_TIME, archiveEntry->datetime().toTime_t());

			listEntry( entry, false);
		}

	listEntry( entry, true);
	finished();
}



/*!
  \fn kio_rarProtocol::stat( const KUrl & url )
  */
void kio_rarProtocol::stat( const KUrl & url )
{
	kDebug(7101) << "kio_rar::stat(const KUrl& url)" << endl ;
	KUrl fileUrl;
	int errorNum;

	errorNum = checkFile( url, fileUrl );
	if( errorNum == 1 )
	{
		// cannot open archive
		return;
	}

	if( errorNum == 2 )
	{
		// it is not a RAR archive
		// send the path to konqueror to redirect to file: kioslave
		redirection( url.path() );
		finished();
		return;
	}

	KIO::UDSEntry entry;

	// check if it is root directory
	if( fileUrl.path( KUrl::RemoveTrailingSlash ) == "/" )
	{
		entry.insert(KIO::UDSEntry::UDS_NAME, "/");
		entry.insert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFDIR);
		statEntry(entry);
		finished();
		return;
	}

	const KArchiveEntry *archiveEntry = m_archiveFile->directory()->entry( fileUrl.path() );

	if( !archiveEntry )
	{
		error( KIO::ERR_DOES_NOT_EXIST, fileUrl.path() );
		return;
	}

	entry.clear();

	entry.insert(KIO::UDSEntry::UDS_NAME, archiveEntry->name());

	entry.insert(KIO::UDSEntry::UDS_FILE_TYPE, archiveEntry->isDirectory() ? S_IFDIR : S_IFREG);

	entry.insert( KIO::UDSEntry::UDS_ACCESS, archiveEntry->permissions() );

	entry.insert( KIO::UDSEntry::UDS_SIZE, archiveEntry->isDirectory() ? 0L : ((KArchiveFile *)archiveEntry)->size() );

	entry.insert( KIO::UDSEntry::UDS_MODIFICATION_TIME, archiveEntry->datetime().toTime_t() );

	statEntry( entry );
	finished();
}


/*!
  \fn kio_rarProtocol::checkFile( const KUrl &url, KUrl &fileUrl )
  */
int kio_rarProtocol::checkFile( const KUrl &url, KUrl &fileUrl )
{
	kDebug(7101) << "kio_rar::checkFile(const KUrl& url, KUrl &fileUrl)" << endl ;
	QString archiveUrl;
	struct stat statbuf;

	if( m_archiveFile && url.path().startsWith( m_archiveFile->fileName() ) )
	{
		fileUrl.setPath(url.path().section( m_archiveFile->fileName(), 1 ));
		// Has it changed ?
		if ( ::stat( QFile::encodeName( m_archiveFile->fileName() ), &statbuf ) == 0 )
		{
			if ( m_archiveTime == statbuf.st_mtime )
				return 0;
		}
		archiveUrl = m_archiveFile->fileName();
	}else{
		if( url.path().indexOf( ".rar", false ) == -1 )
			return 2;
		archiveUrl = url.path().section( ".rar", 0, 0 ) + ".rar";
		if( url.path().endsWith( ".rar" ) )
			fileUrl = "/";
		else
			fileUrl = url.path().section( ".rar", 1 );
	}

	if( m_archiveFile )
	{
		m_archiveFile->close();
		delete m_archiveFile;
	}

	m_archiveFile = new KRar( archiveUrl );
	if( !m_archiveFile->open( QIODevice::ReadOnly ) )
	{
		error( KIO::ERR_CANNOT_OPEN_FOR_READING, archiveUrl );
		return 1;
	}
	::stat( QFile::encodeName( m_archiveFile->fileName() ), &statbuf );
	m_archiveTime = statbuf.st_mtime;
	return 0;
}


/*!
  \fn kio_rarProtocol::receivedData( KProcess *, char* buffer, int len )
  */
void kio_rarProtocol::receivedData()
{
	Q_ASSERT(procShell);
	if (!procShell->bytesAvailable())
	{
		return;
	}
	QByteArray temp = procShell->readAllStandardOutput();
	data(temp);
	processed += temp.length();
	processedSize( processed );
}


/*!
  \fn kio_rarProtocol::del( const KUrl &url, bool isFile )
  */
void kio_rarProtocol::del( const KUrl &url, bool /*isFile*/ )
{
	kDebug(7101) << "kio_rar::del(const KUrl &url, bool isFile)" << endl ;
	KUrl fileUrl;
	int errorNum;

	errorNum = checkFile( url, fileUrl );
	if( errorNum == 1 )
	{
		// cannot open archive
		return;
	}

	if( errorNum == 2 )
	{
		// it is not a RAR archive
		// send the path to konqueror to redirect to file: kioslave
		redirection( url.path() );
		finished();
		return;
	}

	if( !rarProgram.endsWith( "/rar" ) )
	{
		error( KIO::ERR_UNSUPPORTED_ACTION, i18n( "You need to have the rar binary in $PATH to use this action" ) );
		return;
	}

	infoMessage( i18n( "Deleting file..." ) );

	procShell = new KProcess();
	QStringList args;
	args << "d" << m_archiveFile->fileName() << fileUrl.path( KUrl::RemoveTrailingSlash ).remove( 0, 1 );
	procShell->execute(rarProgram, args);

	if( procShell->exitStatus() != QProcess::NormalExit )
		error( KIO::ERR_CANNOT_LAUNCH_PROCESS, url.path() );

	finished();
}

